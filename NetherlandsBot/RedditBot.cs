﻿///This is a bot created by Stepper, using an edited version of RedditSharp to fit my needs. 
/// It also uses a simple Pastebin API for people to see the log. Hallo Nederlanders!

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Net;
using RedditSharp;
using RedditSharp.Things;

namespace NetherlandsBot
{
    class RedditBot
    {
        Reddit redditbot = new Reddit();
        AuthenticatedUser redditbotUser = new AuthenticatedUser();
        List<Post> allPosts = new List<Post>();
        List<CategorizedPost> wantedPost;

        string username;
        string password;
        string postContent;

        string[] KeywordsToSearchFor =  { "Netherlands", "Dutch", "Amsterdam", "Holland" }; //, "Dutch", "Holland", "Amsterdam", "Rotterdam", "North Holland", "South Holland", "North Brabant", "Utrecht", "The Hage"                                                
        string[] SubredditsToIgnore =   { "leagueoflegends", "soccer", "doctorwho", "KotakuInAction", "polandball", "footballmanagergames", "millionairemakers", "personalfinance",
        "needadvice", "askreddit", "celebs" };
        string[] KeywordsToIgnore =     { "Kim Holland", "Willa Holland", "Tom Holland" };
        string[] videoSubreddits =      { "videos" };
        string[] videoLinks =           { "youtube.com", "youtu.be", "vimeo.com", "dailymotion.com", "http://youtube", "https://youtube" };
        string[] newsSubreddits =       { "news", "worldnews" };
        string[] imageSubreddits =      { "gifs", "funny" };
        string[] imageLinks =           { "imgur.com", "puu.sh" };

        public void StartBot()
        {
            Login();
            AcquireSubreddits();
            FormatPost();
            CreatePost();
            Log.PostToPasteBin();
        }

        void Login()
        {
            string[] accountLogin = Program.GetAccountDetails;
        
            username = accountLogin[0];
            password = accountLogin[1];

            Log.Add("Account Login fetched successfully.");
            Log.Add("Splitting account Login.");
            redditbotUser = redditbot.LogIn(username, password);
            if (redditbotUser.Name == String.Empty)
            {
                Log.Add("Logging in failed. Terminating Program.");
                Environment.Exit(1);
            }
            Log.Add("Successfully Logged In!");
        }

        void AcquireSubreddits()
        {
            Log.Add("I'm going to acquire all top posts in the last month.");
            
            List<Post> chosenPosts = new List<Post>();

            foreach(var searchQuery in KeywordsToSearchFor)
            {
                var posts = redditbot.Search<Post>(searchQuery, Sorting.Top, TimeSorting.Month).Take<Post>(25);

                foreach(var post in posts)
                {
                    Log.Add("Found and added: " + post.Title);
                    allPosts.Add(post);
                }
            }
            wantedPost = new List<CategorizedPost>();
            foreach(var post in allPosts)
            {
                
                if(post.NSFW)
                {
                    Log.Add("Ignored post from Subreddit " + post.SubredditName + " because it is NSFW");
                    continue;
                }

                if (KeywordsToIgnore.Any(s=>post.Title.Contains(s)))
                {
                    string ignoredKeyword = KeywordsToIgnore.First(s => post.Title.Contains(s));
                    Log.Add("Ignored post because it had an ignored keyword: " + ignoredKeyword);
                    continue;
                }

                if (SubredditsToIgnore.Any(s=>post.SubredditName.StartsWith(s)))
                {
                    string ignoredSubreddit = SubredditsToIgnore.First(s => post.SubredditName.Contains(s));
                    Log.Add("Ignored post because it had an ignored subreddit: " + ignoredSubreddit);
                    continue;
                }

                if(CrossPost(post))
                    continue;

                if (PostIsNews(post))
                {
                    Log.Add("Categorized post: " + post.Permalink + " as news.");
                    wantedPost.Add(new CategorizedPost(post, PostTypes.NewsArticle));
                }
                else if (post.IsSelfPost)
                {
                    Log.Add("Categorized post: " + post.Permalink + " as self post.");
                    wantedPost.Add(new CategorizedPost(post, PostTypes.SelfPost));
                }
                else if(PostIsVideo(post))
                {
                    Log.Add("Categorized post: " + post.Permalink + " as video.");
                    wantedPost.Add(new CategorizedPost(post, PostTypes.Video));
                }
                else if(PostIsImage(post))
                {
                    Log.Add("Categorized post: " + post.Permalink + " as image.");
                    wantedPost.Add(new CategorizedPost(post, PostTypes.Image));
                }
                
                else
                {
                    Log.Add("Categorized post: " + post.Permalink + " as other.");
                    wantedPost.Add(new CategorizedPost(post, PostTypes.Unkown));
                }
            }
        }


        void FormatPost()
        {
            FileStream fs = new FileStream("defaulttext.txt", FileMode.Open, FileAccess.ReadWrite);
            StreamReader sr = new StreamReader(fs);
            string format = sr.ReadToEnd();
            sr.Close();
            string[] formatArray = Regex.Split(format, @"{SPLIT}");

            postContent = formatArray[0];
            //Post images.
            foreach (var rpost in wantedPost)
            {
                if (rpost.postType == PostTypes.Image)
                {
                    string asdf = "\n[[IMG](" + rpost.post.Url + ")]" + " [" + rpost.post.Title + "](" + rpost.post.Shortlink + ")";

                    foreach(var reference in rpost.GetReference)
                    {
                        asdf += " |||[/r/" + rpost.post.SubredditName + "]";
                    }

                    postContent += asdf + "  ";
                }
            }
            postContent += formatArray[1];
            foreach (var rpost in wantedPost)
            {
                if (rpost.postType == PostTypes.Video)
                {
                    string asdf = "\n[[VID](" + rpost.post.Url + ")]" + " [" + rpost.post.Title + "](" + rpost.post.Shortlink + ")";

                    foreach (var reference in rpost.GetReference)
                    {
                        asdf += " |||[/r/" + rpost.post.SubredditName + "]";
                    }

                    postContent += asdf + "  ";
                }
            }
            postContent += formatArray[2];
            foreach (var rpost in wantedPost)
            {
                if (rpost.postType == PostTypes.NewsArticle)
                {
                    string asdf = "\n[[NEWS](" + rpost.post.Url + ")]" + " [" + rpost.post.Title + "](" + rpost.post.Shortlink + ")";

                    foreach (var reference in rpost.GetReference)
                    {
                        asdf += " |||[/r/" + rpost.post.SubredditName + "]";
                    }

                    postContent += asdf + "  ";
                }
            }
            postContent += formatArray[3];
            foreach (var rpost in wantedPost)
            {
                if (rpost.postType == PostTypes.SelfPost)
                {
                    string asdf = "\n[[SELF](" + rpost.post.Url + ")]" + " [" + rpost.post.Title + "](" + rpost.post.Shortlink + ")";

                    foreach (var reference in rpost.GetReference)
                    {
                        asdf += " |||[/r/" + rpost.post.SubredditName + "]";
                    }

                    postContent += asdf + "  ";

                }
            }
            foreach (var rpost in wantedPost)
            {
                if (rpost.postType == PostTypes.Unkown)
                {
                    string asdf = "\n[[???](" + rpost.post.Url + ")]" + " [" + rpost.post.Title + "](" + rpost.post.Shortlink + ")";

                    foreach (var reference in rpost.GetReference)
                    {
                        asdf += " |||[/r/" + rpost.post.SubredditName + "]";
                    }

                    postContent += asdf + "  ";
                }
            }
            postContent += "______________________________________________________________  \n^I ^am ^a ^bot ^created ^by ^/u/stepepper. ^please ^contact ^me ^if ^you ^have ^suggestions  \n^log ^source ^code";
        }

        void CreatePost()
        {
            var subredditToPostIn = redditbot.GetSubreddit("/r/netherlandsmonthlybot");
            subredditToPostIn.SubmitTextPost("test", postContent);
        }

        #region Post checkersrhaurhaurh AAAAAAA
        bool PostIsVideo(Post post)
        {
            if(videoSubreddits.Any(s=>post.SubredditName.StartsWith(s)))
                return true;
            
            return false;
        }

        bool CrossPost(Post post)
        {
            if(wantedPost.Any(s=>s.post.Title.StartsWith(post.Title)))
            {
                CategorizedPost cat = wantedPost.First(s => s.post.Title.StartsWith(post.Title));
                cat.AddReference( "/r/" + post.SubredditName);
                Log.Add(cat.post.Permalink + " was a crosspost! Reference added!");
                return true;
            }
            else if(wantedPost.Any(s=>s.post.Url == post.Url))
            {
                CategorizedPost cat = wantedPost.First(s=>s.post.Url == post.Url);
                cat.AddReference("/r/" + post.SubredditName);
                Log.Add(cat.post.Permalink + " was a crosspost! Reference added!");
                return true;
            }

            return false;
        }

        


        
        bool PostIsImage(Post post)
        {
            var request = HttpWebRequest.Create(post.Url);
            request.Method = "HEAD";

            try
            {
                using (var response = request.GetResponse())
                {
                    if (response.ContentType.ToLower().StartsWith("image/"))
                        return true;
                }
            }
            catch(WebException e)
            {
                Log.Add("Could not check if post is an image! Error: " + e.Message);
            }

            if (imageSubreddits.Any(s => post.SubredditName.StartsWith(s)))
                return true;

            return false;
        }

        bool PostIsNews(Post post)
        {
            if (newsSubreddits.Any(s => post.SubredditName.StartsWith(s)))
                return true;

            return false;
        }
        #endregion
    }
}
