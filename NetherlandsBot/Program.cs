﻿///This is a bot created by Stepper, using an edited version of RedditSharp to fit my needs. 
/// It also uses a simple Pastebin API for people to see the log. Hallo Nederlanders!

using System;
using System.IO;

namespace NetherlandsBot
{
    class Program
    {
        static RedditBot rb;

        static void Main(string[] args)
        {
            rb = new RedditBot();
            rb.StartBot();
            Console.ReadKey();
        }

        static void Exit(int ExitCode)
        {
            
            Log.PostToPasteBin();
            Environment.Exit(ExitCode);
        }

        static public string[] GetAccountDetails
        {
            get
            {
                try
                {
                    using (StreamReader sr = new StreamReader("accountlogin.txt"))
                    {
                        string accountloginText = sr.ReadToEnd();
                        string[] accountloginText_ar = accountloginText.Split(' ');

                        return accountloginText_ar;
                    }
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine("Failed to read file: " + Environment.NewLine + e.Message);
                }

                
                return null; 
            }
        }

    }
}
