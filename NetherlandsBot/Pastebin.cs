﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Net;

namespace NetherlandsBot
{
    static class Pastebin
    {
        static public string DevKey = string.Empty;
        static readonly string pastebinUrl = "http://pastebin.com/api/api_post.php";
        static readonly string api_option = "paste";
        static readonly string api_paste_format = "text";
        

        static public string Upload(string content)
        {
            if (string.IsNullOrWhiteSpace(content))
                return null;

            NameValueCollection args = new NameValueCollection();

            args.Add("api_dev_key", DevKey);
            args.Add("api_option", api_option);
            args.Add("api_paste_code", content);
            args.Add("api_paste_format", api_paste_format);

            using (WebClient wc = new WebClient())
            {
                string response = Encoding.UTF8.GetString(wc.UploadValues(pastebinUrl, args));
                Console.WriteLine(response);
                Uri isValid = null;
                if(!Uri.TryCreate(response, UriKind.Absolute, out isValid))
                {
                    Log.PasteSucceeded = false;
                    return "Error, paste failed: " + response;
                }

                Log.PasteSucceeded = true;
                return isValid.ToString();
            }
        }
    }
}
