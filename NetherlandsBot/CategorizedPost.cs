﻿using System.Collections.Generic;
using RedditSharp.Things;

namespace NetherlandsBot
{
    class CategorizedPost
    {

        public PostTypes postType;
        public Post post = null;
        List<string> subredditReferences = new List<string>();

        public CategorizedPost(Post post, PostTypes postType)
        {
            this.postType = postType;
            this.post = post;
        }

        public void AddReference(string reference)
        {
            subredditReferences.Add(reference);
        }

        public List<string> GetReference
        {
            get
            {
                return subredditReferences;
            }
        }
    }

    public enum PostTypes
    {
        Unkown,
        SelfPost,
        Image,
        Video,
        NewsArticle
    }
}
