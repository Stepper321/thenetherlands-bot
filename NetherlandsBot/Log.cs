﻿///This is a bot created by Stepper, using an edited version of RedditSharp to fit my needs. 
/// It also uses a simple Pastebin API for people to see the log. Hallo Nederlanders!

using System;
using System.Diagnostics;
using System.IO;

namespace NetherlandsBot
{
    static class Log
    {
        static string filename = DateTime.Now.ToString() + "_LOG.txt";
        static StreamWriter sw;
        static FileStream fileStream;
        static Stopwatch timeSinceLaunch;
        static public bool PasteSucceeded = false;

        static Log()
        {
            string[] accountLogin = Program.GetAccountDetails;
            Console.WriteLine(Pastebin.DevKey);
            Pastebin.DevKey = accountLogin[2];
            timeSinceLaunch = Stopwatch.StartNew();
            filename = filename.Replace(':', '-');
            fileStream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            sw = new StreamWriter(fileStream);
            Add("I am a bot created by /u/stepepper. I post in /r/theNetherlands monthly for mentions of a couple keywords, which you can checkout in the Source Code.");
            Add("This will be my log, where you can check out what I did wrong, or what exactly went wrong. Everything is subject to change and if you have suggestions, please contact my master!");
            Add();
            Add();
        }

        public static void Add(string args = "")
        {
            string stopwatchTime = "[" + timeSinceLaunch.Elapsed.ToString() + "]\t";
            sw.WriteLine(stopwatchTime + args);
            Console.WriteLine(args);
        }

        public static void PostToPasteBin()
        {
            Log.Add("I'm not going to push this log to Pastebin just yet!");
            sw.Close();

            //sw.Close();
            //StreamReader sr = new StreamReader(filename);
            //string PastebinToUpload = sr.ReadToEnd();
            //string paste = Pastebin.Upload(PastebinToUpload);
            //Console.WriteLine(paste);
        }
    }
}
